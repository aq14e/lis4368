> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 - Advanced Web Applications Development

## Ahmad Qureshi

### Project 1 Requirements:

*Three Parts:*

1. Client Validation
2. Giving failed validation ranges
3. Chapter Questions (Chs 9 and 10)

#### README.md file should include the following items:

* Screenshot of Main Page;
* Screenshot of Client Validation;


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of Main Page*:

![Failed Validation](img/main.png)

![Passed Validation](img/main2.png)



