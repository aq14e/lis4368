> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 - Advanced Web Applications Development

## Ahmad Qureshi

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Java/JSP/Servlet Development Installation
3. Chapter Questions (Chs 1 - 4)

#### README.md file should include the following items:

* Screenshot of running java Hello (#1 above);
* Screenshot of running http://localhost:9999 (#2 above, Step #4b in tutorial);
* git commands with short descriptions
* Bitbucket repo links: a) this assignment and b) the completed tutorial above
  (bitbucketstationlocations)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - creates an empty Git repository or reinitializes an existing one
2. git status - shows the working tree status
3. git add - adds file contents to the index
4. git commit - records changes to the repository
5. git push - updates remote refs along with associated objects
6. git pull - fetches from and integrates with another repository or a local branch
7. git config - get and set repository or global options

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of running http://localhost:9999*:

![Apache Tomcat Installation Screenshot](img/tomcat.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/aq14e/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/aq14e/myteamquotes/ "My Team Quotes Tutorial")
