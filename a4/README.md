> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 - Advanced Web Applications Development

## Ahmad Qureshi

### Assignment 4 Requirements:

*Three Parts:*

1. Server Validation
2. Giving failed validation ranges
3. Chapter Questions (Chs 11 and 12)

#### README.md file should include the following items:

* Screenshot of Failed Validation;
* Screenshot of Passed Validation;


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of Main Page*:

![Failed Validation](img/failed.png)

![Passed Validation](img/passed.png)



