> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 - Advanced Web Applications Development

## Ahmad Qureshi

### Assignment 3 Requirements:

*Three Parts:*

1. Developing a database
2. Forward engineering the created database
3. Chapter Questions (Chs 7 and 8)

#### README.md file should include the following items:

* Assessment links of mwb and sql files;
* Screenshot of ERD;


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of ERD*:

![ERD Screenshot](img/a3.png)


#### MWB and SQL Links:

*MWB File*
[a3.mwb](docs/a3.mwb)

*SQL File*
[a3.sql](docs/a3.sql)

 





