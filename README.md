> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**

# LIS 4368 - Advanced Web Applications Development

## Ahmad Qureshi

### LIS 4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install and Configure Git
    - Make Bitbucket Account
    - Install JDK
    - Install Apache Tomcat
    - Provide screenshots of Java Hello and Installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
     - Install and COnfigure MySql
     - Create a new MySql user
     - Develop and deploy a WebApp
     - Deploy servlet using @WebServlet
     - Provide links for each local host URL
     - Provide screenshot of query results

3. [A3 README.md](a3/README.md "My A3 README.md file")
     - Develop and design database
     - Forward Engineer database and export sql file
     - Provide screenshot of ERD

4. [A4 README.md](a4/README.md "My A4 README.md file")
     - Provide server side validation
     - Provide screenshots of failed and working validation
     - Use MVC framework

5. [A5 README.md](a5/README.md "My A5 README.md file")
     - Prevention of SQL injection
     - Communicate directly with database
     - Prevention of XSS

6. [P1 README.md](p1/README.md "My P1 README.md file")
     - Configure Web Application
     - Use Bootstrap Framework
     - Fill out Client Side Validation
     - Fill Carousel with 3 images

7. [P2 README.md](p2/README.md "My P2 README.md file")
     - TBD


