> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 - Advanced Web Applications Development

## Ahmad Qureshi

### Assignment 5 Requirements:

*Three Parts:*

1. Server Validation
2. Giving database results
3. Chapter Questions (Chs 13 - 15)

#### README.md file should include the following items:

* Screenshot of User Validation;
* Screenshot of Passed Validation;
* Screenshot of Database Entry


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of Main Page*:

![User Validation](img/user.png)

![Passed Validation](img/passed.png)

![Database before entry](img/data.png)

![Database after entry](img/data2.png)



