> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 - Advanced Web Applications Development

## Ahmad Qureshi

### Assignment 2 Requirements:

*Three Parts:*

1. MySql installation and User creation
2. Developing and deploying a Web Application
3. Chapter Questions (Chs 5 and 6)

#### README.md file should include the following items:

* Assessment links of local host URL's;
* Screenshot of Query Results;


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Local host Links:

*Main Hello Page*
[http://localhost:9999/hello](http://localhost:9999/hello
 "Hello")

*HelloHome*
[http://localhost:9999/hello/HelloHome.html](http://localhost:9999/hello/HelloHome.html
 "Index")

 *SayHello Servlet*
[http://localhost:9999/hello/sayhello](http://localhost:9999/hello/sayhello
 "SayHello")

 *QueryBook*
[http://localhost:9999/hello/querybook.html](http://localhost:9999/hello/querybook.html
 "QueryBook")

 *SayHi Servlet*
[http://localhost:9999/hello/sayhi](http://localhost:9999/hello/sayhi
 "QueryBook")


#### Assignment Screenshots:

*Screenshot of query results*:

![Query Results Screenshot](img/QueryResults.png)




